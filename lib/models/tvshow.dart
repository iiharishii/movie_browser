class TvShowList {
  int? page;
  int? totalPages;
  int? totalResults;
  List<TvShow>? tvshow;
  TvShowList({
    this.page,
    this.tvshow,
    this.totalPages,
    this.totalResults,
  });

  TvShowList.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    totalResults = json['total_results'];
    totalPages = json['total_pages'];
    if (json['results'] != null) {
      tvshow = [];
      json['results'].forEach((v) {
        tvshow!.add(TvShow.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['page'] = page;
    data['total_results'] = totalResults;
    data['total_pages'] = totalPages;
    if (tvshow != null) {
      data['results'] = tvshow!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TvShow {
  String? backdropPath;
  String? firstAirDate;
  List<int>? genreIds;
  int? id;
  String? name;
  List<String>? originCountry;
  String? originalLanguage;
  String? originalName;
  String? overview;
  double? popularity;
  String? posterPath;
  double? voteAverage;
  int? voteCount;

  TvShow({
    this.backdropPath,
    this.firstAirDate,
    this.genreIds,
    this.id,
    this.name,
    this.originCountry,
    this.originalLanguage,
    this.originalName,
    this.overview,
    this.popularity,
    this.posterPath,
    this.voteAverage,
    this.voteCount,
  });

  factory TvShow.fromJson(Map<String, dynamic> json) => TvShow(
        backdropPath: json["backdrop_path"],
        firstAirDate: json["first_air_date"],
        genreIds: json['genre_ids'].cast<int>(),
        id: json["id"],
        name: json["name"],
        originCountry: json["origin_country"].cast<String>(),
        originalLanguage: json["original_language"],
        originalName: json["original_name"],
        overview: json["overview"],
        popularity: json["popularity"].toDouble(),
        posterPath: json["poster_path"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["backdrop_path"] = backdropPath;
    data["first_air_date"] = firstAirDate;
    data["genre_ids"] = genreIds;
    data["id"] = id;
    data["name"] = name;
    data["origin_country"] = originCountry;
    data["original_language"] = originalLanguage;
    data["original_name"] = originalName;
    data["overview"] = overview;
    data["popularity"] = popularity;
    data["poster_path"] = posterPath;
    data["vote_average"] = voteAverage;
    data["vote_count"] = voteCount;
    return data;
  }
}
