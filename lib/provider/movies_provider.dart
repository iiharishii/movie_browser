import 'package:flutter/material.dart';
import 'package:moview_browser/models/tvshow.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api-services/api_services.dart';
import '../api-services/endpoints.dart';
import '../models/movie.dart';

class MovieTvShowProvider extends ChangeNotifier {
  List<Movie>? moviesDiscoverList;
  List<TvShow>? tvShowList;
  List<Movie>? movieTopratedList;
  List<Movie>? movieTrendingList;
  MovieTvShowProvider.init() {
    movieProviderDiscoverList();
    tvProviderTvShowList();
    movieProviderTrendingList();
    movieProviderTopRatedList();
  }

  movieProviderDiscoverList() {
    fetchMovies(Endpoints.discoverMoviesUrl(1)).then((value) {
      moviesDiscoverList = value;

      notifyListeners();
    });
  }

  movieProviderTopRatedList() {
    fetchMovies(Endpoints.topRatedUrl(1)).then((value) {
      movieTopratedList = value;
      notifyListeners();
    });
  }

  movieProviderTrendingList() {
    fetchMovies(Endpoints.trendingMoviesUrl(1)).then((value) {
      movieTrendingList = value;
      notifyListeners();
    });
  }

  tvProviderTvShowList() {
    fetchTvShow(Endpoints.tvShow(1)).then((value) {
      tvShowList = value;
      notifyListeners();
    });
  }
}
