import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/app_theme.dart';

import '../provider/movies_provider.dart';
import 'movie_detail.dart';

class TrendingMovies extends StatefulWidget {
  final ThemeData themeData;
  final String? title;
  const TrendingMovies({
    Key? key,
    required this.themeData,
    this.title,
  }) : super(key: key);
  @override
  _TrendingMoviesState createState() => _TrendingMoviesState();
}

class _TrendingMoviesState extends State<TrendingMovies> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final movieTvShowProvider = Provider.of<MovieTvShowProvider>(context);
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(widget.title!,
                  style: widget.themeData.textTheme.headline5),
            ),
          ],
        ),
        SizedBox(
          width: double.infinity,
          height: 200,
          child: movieTvShowProvider.movieTrendingList == null
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: movieTvShowProvider.movieTrendingList!.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MovieDetailPage(
                                      movie: movieTvShowProvider
                                          .movieTrendingList![index],
                                      themeData: widget.themeData,
                                      heroId:
                                          '${movieTvShowProvider.movieTrendingList![index].id}${widget.title}')));
                        },
                        child: Hero(
                          tag:
                              '${movieTvShowProvider.movieTrendingList![index].id}${widget.title}',
                          child: SizedBox(
                            width: 100,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: FadeInImage(
                                      image: NetworkImage(imageUrl +
                                          'w500/' +
                                          movieTvShowProvider
                                              .movieTrendingList![index]
                                              .posterPath!),
                                      fit: BoxFit.cover,
                                      placeholder: const AssetImage(
                                          'assets/images/loading.gif'),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    movieTvShowProvider
                                        .movieTrendingList![index].title!,
                                    style: widget.themeData.textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
        ),
      ],
    );
  }
}
