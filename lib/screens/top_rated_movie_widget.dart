import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/app_theme.dart';

import '../provider/movies_provider.dart';
import 'movie_detail.dart';

class TopRatedMovies extends StatefulWidget {
  final ThemeData themeData;
  final String? title;
  const TopRatedMovies({
    Key? key,
    required this.themeData,
    this.title,
  }) : super(key: key);
  @override
  _TopRatedMoviesState createState() => _TopRatedMoviesState();
}

class _TopRatedMoviesState extends State<TopRatedMovies> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final movieTvShowProvider = Provider.of<MovieTvShowProvider>(context);
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(widget.title!,
                  style: widget.themeData.textTheme.headline5),
            ),
          ],
        ),
        SizedBox(
          width: double.infinity,
          height: 200,
          child: movieTvShowProvider.movieTopratedList == null
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: movieTvShowProvider.movieTopratedList!.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MovieDetailPage(
                                      movie: movieTvShowProvider
                                          .movieTopratedList![index],
                                      themeData: widget.themeData,
                                      heroId:
                                          '${movieTvShowProvider.movieTopratedList![index].id}${widget.title}')));
                        },
                        child: Hero(
                          tag:
                              '${movieTvShowProvider.movieTopratedList![index].id}${widget.title}',
                          child: SizedBox(
                            width: 100,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: FadeInImage(
                                      image: NetworkImage(imageUrl +
                                          'w500/' +
                                          movieTvShowProvider
                                              .movieTopratedList![index]
                                              .posterPath!),
                                      fit: BoxFit.cover,
                                      placeholder: const AssetImage(
                                          'assets/images/loading.gif'),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    movieTvShowProvider
                                        .movieTopratedList![index].title!,
                                    style: widget.themeData.textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
        ),
      ],
    );
  }
}
