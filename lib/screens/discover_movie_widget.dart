import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/app_theme.dart';

import '../provider/movies_provider.dart';
import 'movie_detail.dart';

class DiscoverMovies extends StatefulWidget {
  final ThemeData themeData;
  const DiscoverMovies({
    Key? key,
    required this.themeData,
  }) : super(key: key);
  @override
  _DiscoverMoviesState createState() => _DiscoverMoviesState();
}

class _DiscoverMoviesState extends State<DiscoverMovies> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final movieProvider = Provider.of<MovieTvShowProvider>(context);

    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  Text('Discover', style: widget.themeData.textTheme.headline5),
            ),
          ],
        ),
        SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.width,
          child: movieProvider.moviesDiscoverList == null
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : CarouselSlider.builder(
                  options: CarouselOptions(
                    disableCenter: true,
                    viewportFraction: 0.8,
                    enlargeCenterPage: true,
                  ),
                  itemBuilder:
                      (BuildContext context, int index, pageViewIndex) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MovieDetailPage(
                                    movie: movieProvider
                                        .moviesDiscoverList![index],
                                    themeData: widget.themeData,
                                    heroId:
                                        '${movieProvider.moviesDiscoverList![index].id}discover')));
                      },
                      child: Hero(
                        tag:
                            '${movieProvider.moviesDiscoverList![index].id}discover',
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: FadeInImage(
                            image: NetworkImage(imageUrl +
                                'w500/' +
                                movieProvider
                                    .moviesDiscoverList![index].posterPath!),
                            fit: BoxFit.cover,
                            placeholder:
                                const AssetImage('assets/images/loading.gif'),
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: movieProvider.moviesDiscoverList!.length,
                ),
        ),
      ],
    );
  }
}
