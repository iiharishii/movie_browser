import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/tvshow.dart';
import '../models/movie.dart';

Future<List<Movie>> fetchMovies(String api) async {
  MovieList movieList;
  var res = await http.get(Uri.parse(api));
  var decodeRes = jsonDecode(res.body);
  movieList = MovieList.fromJson(decodeRes);
  return movieList.movies ?? [];
}

Future<List<TvShow>> fetchTvShow(String api) async {
  TvShowList tvShowList;
  var res = await http.get(Uri.parse(api));
  var decodeRes = jsonDecode(res.body);
  tvShowList = TvShowList.fromJson(decodeRes);
  return tvShowList.tvshow ?? [];
}
