import '../common/app_theme.dart';

class Endpoints {
  static String discoverMoviesUrl(int page) {
    return '$baseUrl'
        '/discover/movie?api_key='
        '$apiKey'
        '&language=en-US&sort_by=popularity'
        '.desc&include_adult=false&include_video=false&page'
        '=$page';
  }

  static String trendingMoviesUrl(int page) {
    return '$baseUrl'
        '/movie/now_playing?api_key='
        '$apiKey'
        '&include_adult=false&page=$page';
  }

  static String tvShow(int page) {
    return '$baseUrl'
        '/tv/top_rated?api_key='
        '$apiKey'
        '&include_adult=false&page=$page';
  }

  static String topRatedUrl(int page) {
    return '$baseUrl'
        '/movie/top_rated?api_key='
        '$apiKey'
        '&include_adult=false&page=$page';
  }

  static String movieSearchUrl(String query) {
    return "$baseUrl/search/movie?query=$query&api_key=$apiKey";
  }
}
