import 'package:flutter/material.dart';
import 'package:moview_browser/provider/movies_provider.dart';
import 'package:moview_browser/screens/trending_movie_widget.dart';
import 'package:moview_browser/screens/tv_show_widget.dart';
import '../models/movie.dart';
import '../screens/movie_detail.dart';
import '../screens/search_view.dart';
import '../screens/settings.dart';
import 'screens/search_movie_widget.dart';
import 'provider/theme_state_provider.dart';
import 'package:provider/provider.dart';

import 'api-services/endpoints.dart';
import 'screens/discover_movie_widget.dart';
import 'screens/top_rated_movie_widget.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<ThemeStateProvider>(
            create: (context) => ThemeStateProvider.init(),
          ),
          ChangeNotifierProvider<MovieTvShowProvider>(
            create: (context) => MovieTvShowProvider.init(),
          ),
        ],
        builder: (context, snapshot) {
          return MaterialApp(
            title: 'Movie Browser',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                primarySwatch: Colors.blue, canvasColor: Colors.transparent),
            home: const MyHomePage(),
          );
        });
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final state = Provider.of<ThemeStateProvider>(context);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: state.themeData.colorScheme.secondary,
          ),
          onPressed: () {
            _scaffoldKey.currentState?.openDrawer();
          },
        ),
        centerTitle: true,
        title: Text(
          'Movie Browser',
          style: state.themeData.textTheme.headline5,
        ),
        backgroundColor: state.themeData.primaryColor,
        actions: <Widget>[
          IconButton(
            color: state.themeData.colorScheme.secondary,
            icon: const Icon(Icons.search),
            onPressed: () async {
              final Movie? result = await showSearch<Movie?>(
                  context: context,
                  delegate: MovieSearch(
                    themeData: state.themeData,
                  ));
              if (result != null) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MovieDetailPage(
                            movie: result,
                            themeData: state.themeData,
                            heroId: '${result.id}search')));
              }
            },
          )
        ],
      ),
      drawer: const Drawer(
        child: SettingsPage(),
      ),
      body: Container(
        color: state.themeData.primaryColor,
        child: ListView(
          children: <Widget>[
            DiscoverMovies(
              themeData: state.themeData,
            ),
            TvShow(
              themeData: state.themeData,
              title: 'TV Show',
            ),
            TopRatedMovies(
              themeData: state.themeData,
              title: 'Top Rated',
            ),
            TrendingMovies(
              themeData: state.themeData,
              title: 'Trending',
            ),
          ],
        ),
      ),
    );
  }
}
